Why? What?
==========

I'm making a very light-weight internal network service, and am using
the amazing flask, as it's excellent for prototyping such things.

However, I feel like it's a little over-weight for the simplicity of the
task.  So, out of curiosity, I'm trying to make a flask-alike server
and WSGI microframework which has just enough for these types of projects.

It's not intended to replace flask, obviously.  There's no point.

But it is intended that for things like this which don't need a templating
language, or any of that, it should be close enough to flask that I can
drop this in, and with minimal changes have it running very quickly.

current state:
==============

very very very initial alpha. For templating, it's using pythons string.format instead of
jinja2.

Routes currently don't use variables, but are just a plain lookup.

static files seem to be (basically) working.

All that said:

It's quite fast. (Ha, no surprise, with all those caveats...)

broken stuff:
=============

- the 'request' object is just the pure WSGI environ at the moment.
- MIME types for templates (and static files)
- routing <with><vars>

basic speed comparison:
=======================

just a very quick comparison, in a dead simple 2-views monitoring app using apache bench to get 10000 requests,
both running with the gevent server:

=========== ======= =============
     what   flask   espressocup
=========== ======= =============
total time: 30.316  6.748
req/sec:    329.85  1481.94
req time:   30.316  6.748
xfer rate:  1120.68 4752.62
=========== ======= =============

obviously, these numbers are not going to always hold true, but it does at least prove that for very very simple
apps that don't need full flask power, a flask-subset may well be enough, and heck of a lot faster.  More benchmarks
are needed to see which parts of flask are being slow here - it could be that we can add jinja2 back in if needed
and still get good improvements.  Then again, it could be that just replacing jinja2 with simpler string.formatting
is enough to get flask way way faster - which would be a nice thing to know. :-)
